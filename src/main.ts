import Vue from "vue";
import App from "./App.vue";
import VueMeta from "vue-meta";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;
Vue.config.ignoredElements = [
  /^ion-/, // ionicon 沒有註冊，所以直接使用在template內vue會報未註冊組件錯誤
];

Vue.use(VueMeta);
Vue.prototype.$$store = store; // 為了型別.. ㄋ

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
