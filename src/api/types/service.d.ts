export interface ApiQuery {
  $select?: string;
  $filter?: string;
  $orderby?: string;
  $top?: number;
  $skip?: string;
  $format?: "json" | "xml";
}
