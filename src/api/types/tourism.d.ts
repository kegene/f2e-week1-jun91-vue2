import { QueryType } from "@/store";
import { ApiQuery } from "./service";

export interface Query extends ApiQuery {
  $spatialFilter?: string;
  $health?: boolean;
  city?: string;
}
export interface ScenicSpotInfo {
  /** 景點代碼 */
  ScenicSpotID: string;
  /** 景點名稱 */
  ScenicSpotName: string;
  /** 景點特色詳細說明 */
  DescriptionDetail: string;
  /** 景點特色精簡說明 */
  Description: string;
  /** 景點服務電話 */
  Phone: string;
  /** 景點地址 */
  Address: string;
  /** 郵遞區號 */
  ZipCode: string;
  /** 交通資訊 */
  TravelInfo: string;
  /** 開放時間 */
  OpenTime: string;
  /** 景點照片 */
  Picture: Picture;
  /** 景點地圖/簡圖介紹網址 */
  MapUrl: string;
  /** 景點位置 */
  Position: Position;
  /** 景點分類1 */
  Class1: string;
  /** 景點分類2 */
  Class2: string;
  /** 景點分類3 */
  Class3: string;
  /** 古蹟分級 */
  Level: string;
  /** 景點官方網站網址 */
  WebsiteUrl: string;
  /** 停車資訊 */
  ParkingInfo: string;
  /** 景點主要停車場位置 */
  ParkingPosition: Position;
  /** 票價資訊 */
  TicketInfo: string;
  /** 警告及注意事項 */
  Remarks: string;
  /** 常用搜尋關鍵字 */
  Keyword: string;
  /** 所屬縣市 */
  City: string;
  /** 觀光局檔案更新時間 */
  SrcUpdateTime: Date;
  /** 本平台資料更新時間 */
  UpdateTime: Date;
}

export interface RestaurantInfo {
  /** 餐飲店家代碼 */
  RestaurantID: string;
  /** 餐飲店家名稱 */
  RestaurantName: string;
  /** 店家簡述 */
  Description: string;
  /** 店家地址 */
  Address: string;
  /** 郵遞區號 */
  ZipCode: string;
  /** 預約電話 */
  Phone: string;
  /** 營業時間 */
  OpenTime: string;
  /** 店家網站網址 */
  WebsiteUrl: string;
  /** 店家照片 */
  Picture: Picture;
  /** 店家位置 */
  Position: Position;
  /** 店家分類 */
  Class: string;
  /** 店家地圖/簡圖介紹網址 */
  MapUrl: string;
  /** 停車資訊 */
  ParkingInfo: string;
  /** 所屬縣市 */
  City: string;
  /** 觀光局檔案更新時間(ISO8601格式:yyyy-MM-ddTHH:mm:sszzz) */
  SrcUpdateTime: string;
  /** 本平台資料更新時間(ISO8601格式:yyyy-MM-ddTHH:mm:sszzz) */
  UpdateTime: string;
}
export interface HotelInfo {
  /** 旅館民宿代碼 */
  HotelID: string;
  /** 旅館民宿名稱 */
  HotelName: string;
  /** 旅館民宿簡述 */
  Description: string;
  /** 觀光旅館星級 */
  Grade: string;
  /** 旅館民宿地址 */
  Address: string;
  /** 郵遞區號 */
  ZipCode: string;
  /** 旅館民宿電話 */
  Phone: string;
  /** 旅館民宿傳真 */
  Fax: string;
  /** 旅館民宿網站網址 */
  WebsiteUrl: string;
  /** 旅館民宿照片 */
  Picture: Picture;
  /** 旅館民宿位置 */
  Position: Position;
  /** 旅館民宿分類 */
  Class: string;
  /** 旅館民宿地點簡圖連結網址 */
  MapUrl: string;
  /** 房型、價目及數量說明 */
  Spec: string; //
  /** 服務內容介紹 */
  ServiceInfo: string;
  /** 停車資訊 */
  ParkingInfo: string;
  /** 所屬縣市 */
  City: string;
  /** 觀光局檔案更新時間(ISO8601格式:yyyy-MM-ddTHH:mm:sszzz) */
  SrcUpdateTime: string; //
  /** 本平台資料更新時間(ISO8601格式:yyyy-MM-ddTHH:mm:sszzz) */
  UpdateTime: string;
}

export interface ActivityInfo {
  /** 活動訊息代碼 */
  ActivityID: string;
  /** 活動名稱 */
  ActivityName: string;
  /** 活動簡述 */
  Description: string;
  /** 活動參與對象 */
  Particpation: string;
  /** 主要活動地點名稱 */
  Location: string;
  /** 主要活動地點地址 */
  Address: string;
  /** 活動聯絡電話 */
  Phone: string;
  /** 活動主辦單位 */
  Organizer: string;
  /** 活動開始時間(ISO8601格式:yyyy-MM-ddTHH:mm:sszzz) */
  StartTime: string;
  /** 活動結束時間(ISO8601格式:yyyy-MM-ddTHH:mm:sszzz) */
  EndTime: string;
  /** 週期性活動執行時間 */
  Cycle: string;
  /** 非週期性活動執行時間 */
  NonCycle: string;
  /** 活動網址 */
  WebsiteUrl: string;
  /** 活動照片 */
  Picture: Picture;
  /** 活動位置 */
  Position: Position;
  /** 活動分類1 */
  Class1: string;
  /** 活動分類2 */
  Class2: string;
  /** 活動地圖/簡圖連結網址 */
  MapUrl: string;
  /** 交通資訊 */
  TravelInfo: string;
  /** 停車資訊 */
  ParkingInfo: string;
  /** 費用標示 */
  Charge: string;
  /** 備註(其他活動相關事項) */
  Remarks: string;
  /** 所屬縣市 */
  City: string;
  /** 觀光局檔案更新時間(ISO8601格式:yyyy-MM-ddTHH:mm:sszzz) */
  SrcUpdateTime: string;
  /**  本平台資料更新時間(ISO8601格式:yyyy-MM-ddTHH:mm:sszzz) */
  UpdateTime: string;
}
export interface Picture {
  PictureUrl1: string; // 照片連結網址1
  PictureDescription1: string; // 照片說明1
  PictureUrl2: string; // 照片連結網址2
  PictureDescription2: string; // 照片說明2
  PictureUrl3: string; // 照片連結網址3
  PictureDescription3: string; // 照片說明3
}

export interface Position {
  /** 位置緯度(WGS84) */
  PositionLon: number;
  /** 位置緯度(WGS84) */
  PositionLat: number;
  /** 地理空間編碼 */
  GeoHash: string;
}
