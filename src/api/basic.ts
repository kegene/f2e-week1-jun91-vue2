import useAxios, { AxiosPromise } from "@/utils/axios";

import { getAuthorizationHeader } from "./utils/auth";

import * as Basic from "./types/basic";

const $axios = useAxios({
  baseURL: "https://link.motc.gov.tw/v2/Basic",
  headers: getAuthorizationHeader(),
});

export default {
  getCountryList(query: Basic.CityQuery): AxiosPromise<Basic.TownResponse> {
    return $axios.get("/City", {
      params: query,
    });
  },
  getTownList(query: Basic.TownQuery): AxiosPromise<Basic.TownResponse> {
    const { City, ...other } = query;
    return $axios.get(`/City/${City}/Town`, {
      data: { ...other },
    });
  },
};
