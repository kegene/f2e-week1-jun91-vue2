import { Query, Picture } from "../types/tourism";
import { City, Town } from "../types/basic";

export default {
  getAddress: (country?: City, town?: Town) => {
    if (!country || country.City === "") return;
    return [country?.CityName, town?.TownCode ? town.TownName : undefined]
      .filter((item) => !!item)
      .map((value) => `contains(Address,'${value}')`)
      .join("\u0020and\u0020");
  },
  getKeyword: (keyword: string, propKey = ["Name", "Description"]) => {
    if (!keyword) {
      return;
    }
    return propKey
      .map((key) => `contains(${key},'${keyword}')`)
      .join("\u0020or\u0020");
  },
};
export const useQuery = (params: Partial<Query>): Query => {
  return {
    ...params,
    $format: "json",
  };
};

export const formatPicture = (Picture: Picture) => {
  const keys = Object.keys(Picture);

  const obj: Record<string, any> = {};
  for (const key of keys) {
    const index = key.replace(/PictureUrl|PictureDescription/, "");
    if (!obj[index]) {
      obj[index] = {};
    }
    const name = key.replace(/[\d+]/g, "");
    obj[index][name] = Picture[key as keyof Picture];
  }

  return Object.values(obj).map(
    ({ PictureUrl: url, PictureDescription: description }) => ({
      url,
      description,
    })
  );
};
