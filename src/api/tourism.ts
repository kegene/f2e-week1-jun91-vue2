import useAxios from "@/utils/axios";

import { getAuthorizationHeader } from "./utils/auth";

import * as Tourism from "./types/tourism";

const $axios = useAxios({
  baseURL: "https://ptx.transportdata.tw/MOTC/v2/Tourism",
  headers: getAuthorizationHeader(),
});

export default {
  /**
   * 取得所有/指定觀光景點資料
   */
  getScenicSpotList(query: Tourism.Query): Promise<Tourism.ScenicSpotInfo[]> {
    const { city = "", ...other } = query;
    return $axios
      .get("/ScenicSpot/{city}".replace(/\/{city}/, `/${city}`), {
        params: { ...other },
      })
      .then((res) => res.data);
  },
  /**
   * 取得所有/指定觀光餐飲資料
   */
  getRestaurantList(query: Tourism.Query): Promise<Tourism.RestaurantInfo[]> {
    const { city = "", ...other } = query;
    return $axios
      .get("/Restaurant/{city}".replace(/\/{city}/, `/${city}`), {
        params: { ...other },
      })
      .then((res) => res.data);
  },
  /**
   * 取得所有/指定觀光旅宿資料
   */
  getHotelList(query: Tourism.Query): Promise<Tourism.HotelInfo[]> {
    const { city = "", ...other } = query;
    return $axios
      .get("/Hotel/{city}".replace(/\/{city}/, `/${city}`), {
        params: { ...other },
      })
      .then((res) => res.data);
  },
  /**
   * 取得所有/指定活動資料
   */
  getActivityList(query: Tourism.Query): Promise<Tourism.ActivityInfo[]> {
    const { city = "", ...other } = query;
    return $axios
      .get("/Activity/{city}".replace(/\/{city}/, `/${city}`), {
        params: { ...other },
      })
      .then((res) => res.data);
  },
};
