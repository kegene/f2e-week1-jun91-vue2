import Vue, { ComponentOptions } from "vue";
import { MetaInfo, MetaInfoComputed, VueMetaPlugin } from "./vue-meta";
import { Store } from "vuex";
import { StoreType } from "./store";

declare module "vue/types/vue" {
  interface Vue {
    $meta(): VueMetaPlugin;
    $$store: Store<StoreType>;
  }
}

declare module "vue/types/options" {
  interface ComponentOptions<V extends Vue> {
    metaInfo?: MetaInfo | MetaInfoComputed;
  }
}
