import * as Tourism from "@/api/types/tourism";

export type ScenicSpotInfo = Pick<
  Tourism.ScenicSpotInfo,
  | "ScenicSpotID"
  | "ScenicSpotName"
  | "DescriptionDetail"
  | "Description"
  | "Phone"
  | "Address"
  | "ZipCode"
  | "TravelInfo"
  | "Picture"
  | "OpenTime"
  | "TicketInfo"
  | "Position"
>;

export type RestaurantInfo = Pick<
  Tourism.RestaurantInfo,
  | "RestaurantID"
  | "RestaurantName"
  | "Description"
  | "Phone"
  | "Address"
  | "ZipCode"
  | "WebsiteUrl"
  | "Picture"
  | "OpenTime"
  | "Class"
  | "Position"
>;

export type HotelInfo = Pick<
  Tourism.HotelInfo,
  | "HotelID"
  | "HotelName"
  | "Description"
  | "Grade"
  | "Phone"
  | "Address"
  | "ZipCode"
  | "WebsiteUrl"
  | "Picture"
  | "Spec"
  | "Class"
  | "Position"
>;

export type ActivityInfo = Pick<
  Tourism.ActivityInfo,
  | "ActivityID"
  | "ActivityName"
  | "Description"
  | "Phone"
  | "Organizer"
  | "Address"
  | "WebsiteUrl"
  | "Picture"
  | "Class1"
  | "Class2"
  | "Position"
  | "StartTime"
  | "EndTime"
>;
