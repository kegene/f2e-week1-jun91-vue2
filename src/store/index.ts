import Vue from "vue";
import Vuex from "vuex";

import * as api from "@/api";
import { formatPicture } from "@/api/utils/query";
import * as Tourism from "@/api/types/tourism";
import * as Basic from "@/api/types/basic";
import * as State from "./state";
import * as Getters from "./getters-state";

import queryUtils, { useQuery } from "@/api/utils/query";

export interface QueryType {
  country: Basic.City;
  town: Basic.Town;
  keyword: string;
  pageIndex: number;
}
export interface StoreType {
  scenicSpotList: State.ScenicSpotInfo[];
  scenicSpotParams: Tourism.Query;

  restaurantList: State.RestaurantInfo[];
  restaurantParams: Tourism.Query;

  hotelList: State.HotelInfo[];
  hotelParams: Tourism.Query;

  activityList: State.ActivityInfo[];
  activityParams: Tourism.Query;

  query: QueryType;
}

export class Country implements Basic.City {
  public City = "";
  public CityCode = "";
  public CityID = "";
  public CityName = "";
  public CountyID = "";
  public Version = "";
}
export class Town implements Basic.Town {
  public TownCode = "";
  public TownName = "";
  public Version = "";
  public CityName = "";
}
export class Query implements QueryType {
  public country: Basic.City = new Country();
  public town: Basic.Town = new Town();
  public keyword = "";
  public pageIndex = 1;
}

export interface ActionQuery {
  params: Query;
  isAdd: boolean;
}
export interface MutationsPayload<T> {
  data: T;
  isAdd: boolean;
}

Vue.use(Vuex);

export default new Vuex.Store<StoreType>({
  state: {
    scenicSpotList: [],
    scenicSpotParams: {
      $format: "json",
    },
    restaurantList: [],
    restaurantParams: {
      $format: "json",
    },
    hotelList: [],
    hotelParams: {
      $format: "json",
    },
    activityList: [],
    activityParams: {
      $format: "json",
    },
    query: new Query(),
  },
  getters: {
    formatScenicSpotList(state): Getters.ScenicSpotInfo[] {
      return state.scenicSpotList.map((item) => {
        return {
          ...item,
          Picture: formatPicture(item.Picture),
        };
      });
    },
    formatRestaurantList(state): Getters.RestaurantInfo[] {
      return state.restaurantList.map((item) => {
        return {
          ...item,
          Picture: formatPicture(item.Picture),
        };
      });
    },
    formatHotelList(state): Getters.HotelInfo[] {
      return state.hotelList.map((item) => {
        return {
          ...item,
          Picture: formatPicture(item.Picture),
        };
      });
    },
    formatActivityList(state): Getters.ActivityInfo[] {
      return state.activityList.map((item) => {
        return {
          ...item,
          Picture: formatPicture(item.Picture),
        };
      });
    },
  },
  mutations: {
    setScenicSpotList(
      state,
      payload: MutationsPayload<State.ScenicSpotInfo[]>
    ) {
      state.scenicSpotList = payload.isAdd
        ? [...state.scenicSpotList, ...payload.data]
        : payload.data;
    },
    setRestaurantList(
      state,
      payload: MutationsPayload<State.RestaurantInfo[]>
    ) {
      state.restaurantList = payload.isAdd
        ? [...state.restaurantList, ...payload.data]
        : payload.data;
    },
    setHotelList(state, payload: MutationsPayload<State.HotelInfo[]>) {
      state.hotelList = payload.isAdd
        ? [...state.hotelList, ...payload.data]
        : payload.data;
    },
    setActivityList(state, payload: MutationsPayload<State.ActivityInfo[]>) {
      state.activityList = payload.isAdd
        ? [...state.activityList, ...payload.data]
        : payload.data;
    },
    setQuery(state, payload: Query) {
      state.query = {
        country: payload.country || new Country(),
        town: payload.town || new Town(),
        keyword: payload.keyword || "",
        pageIndex: payload.pageIndex || 1,
      };
    },
    setParams(
      state,
      payload: {
        keyName: keyof Pick<
          StoreType,
          "hotelParams" | "restaurantParams" | "scenicSpotParams"
        >;
        $params: Tourism.Query;
      } & QueryType
    ) {
      const { keyName, country, town, keyword, $params } = payload;
      const { $select } = state[keyName];

      const params = useQuery({
        $top: 12,
        ...$params,
        $filter: "",
      });
      const addressValue = queryUtils.getAddress(country, town);
      const keywordValue = queryUtils.getKeyword(keyword, $select?.split(","));
      params.$filter += addressValue ? `(${addressValue})` : "";
      params.$filter += keywordValue
        ? `${addressValue ? "and" : ""} (${keywordValue})`
        : "";
      if (!params.$filter) {
        params.$filter = undefined;
      }
      state[keyName] = params;
    },
  },
  actions: {
    async getScenicSpotList({ state, commit }, { params, isAdd }: ActionQuery) {
      commit("setParams", {
        keyName: "scenicSpotParams",
        $params: {
          $select: `ScenicSpotID,ScenicSpotName,DescriptionDetail,Description,Phone,Address,ZipCode,TravelInfo,OpenTime,Picture,TicketInfo,Position`,
          $skip: (params.pageIndex - 1) * 12,
        },
        ...params,
      });
      commit("setScenicSpotList", {
        data: await api.Tourism.getScenicSpotList(state.scenicSpotParams),
        isAdd,
      });
    },
    async getRestaurantList({ state, commit }, { params, isAdd }: ActionQuery) {
      commit("setParams", {
        keyName: "restaurantParams",
        $params: {
          $select: `RestaurantID,RestaurantName,Description,Phone,Address,ZipCode,WebsiteUrl,OpenTime,Picture,Class,Position`,
          $skip: (params.pageIndex - 1) * 12,
        },
        ...params,
      });
      commit("setRestaurantList", {
        data: await api.Tourism.getRestaurantList(state.restaurantParams),
        isAdd,
      });
    },
    async getHotelList({ state, commit }, { params, isAdd }: ActionQuery) {
      commit("setParams", {
        keyName: "hotelParams",
        $params: {
          $select: `HotelID,HotelName,Description,Grade,Phone,Address,ZipCode,WebsiteUrl,Picture,Spec,Class,Position`,
          $skip: (params.pageIndex - 1) * 12,
        },
        ...params,
      });
      commit("setHotelList", {
        data: await api.Tourism.getHotelList(state.hotelParams),
        isAdd,
      });
    },
    async getActivityList({ state, commit }, { params, isAdd }: ActionQuery) {
      commit("setParams", {
        keyName: "activityParams",
        $params: {
          $select: `ActivityID,ActivityName,Description,Phone,Organizer,Address,WebsiteUrl,Picture,Class1,Class2,Position,StartTime,EndTime`,
          $skip: (params.pageIndex - 1) * 12,
        },
        ...params,
      });
      commit("setActivityList", {
        data: await api.Tourism.getActivityList(state.activityParams),
        isAdd,
      });
    },
    async getActivityAllList({ state, commit }, { params }: { params: Query }) {
      commit("setParams", {
        keyName: "activityParams",
        $params: {
          $select: `ActivityID,ActivityName,Description,Phone,Organizer,Address,WebsiteUrl,Picture,Class1,Class2,Position,StartTime,EndTime`,
          $top: undefined,
        },
        ...params,
      });
      commit("setActivityList", {
        data: await api.Tourism.getActivityList(state.activityParams),
      });
    },
  },
  modules: {},
});
