import * as State from "./state";

export interface ScenicSpotInfo extends Omit<State.ScenicSpotInfo, "Picture"> {
  Picture: Picture[];
}

export interface RestaurantInfo extends Omit<State.RestaurantInfo, "Picture"> {
  Picture: Picture[];
}

export interface HotelInfo extends Omit<State.HotelInfo, "Picture"> {
  Picture: Picture[];
}

export interface ActivityInfo extends Omit<State.ActivityInfo, "Picture"> {
  Picture: Picture[];
}

export interface Picture {
  url: string;
  description: string;
}
