declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}
declare module "*.jpg" {
  const jpg: any;
  export default jpg;
}
declare module "*.png" {
  const png: any;
  export default png;
}
