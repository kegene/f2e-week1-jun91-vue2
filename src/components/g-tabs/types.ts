import type Vue from "vue";
import type { VNode } from "vue";

export interface TabComponent extends Vue {
  /** required 綁定的key name  */
  name: string;
  /** tab 標題  */
  label: string;
  /** 禁用選單  */
  disabled: boolean;
}
export type TabComponentPropsData = Pick<
  TabComponent,
  "name" | "label" | "disabled"
>;

export declare interface GTabsProps {
  /** active name，v-model綁定的  */
  value?: string; // activeName
  /** 大小尺寸，剛發現設計稿只有一種尺寸，多弄了  */
  size: "default" | "smail";
  /** 主題色  */
  theme: "white" | "dark";

  /** true => 切換, false => 不切換 */
  beforeChange: (tab: VNode) => boolean;
}
