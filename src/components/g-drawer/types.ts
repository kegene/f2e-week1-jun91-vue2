import type Vue from "vue";

export declare class GDrawer extends Vue {
  // 先這樣.. 其他屬性有時間再補齊
  scrollTo(options: ScrollToOptions): void;
}

export interface ScrollToOptions extends ScrollOptions {
  left?: number;
  top?: number;
}

export interface ScrollOptions {
  behavior?: ScrollBehavior;
}
