import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/home/main.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/search",
    name: "search",
    props: {
      country: undefined,
      town: undefined,
      keyword: undefined,
    },
    component: () =>
      import(/* webpackChunkName: "search" */ "../views/home-search.vue"),
  },
  {
    path: "/scenic-spot",
    name: "scenic-spot",
    component: () =>
      import(/* webpackChunkName: "scenic-spot" */ "../views/scenic-spot.vue"),
  },
  {
    path: "/restaurant",
    name: "restaurant",
    component: () =>
      import(/* webpackChunkName: "restaurant" */ "../views/restaurant.vue"),
  },
  {
    path: "/hotel",
    name: "hotel",
    component: () =>
      import(/* webpackChunkName: "hotel" */ "../views/hotel.vue"),
  },
  {
    path: "/activity",
    name: "activity",
    component: () =>
      import(/* webpackChunkName: "activity" */ "../views/activity.vue"),
  },
  {
    path: "/journey",
    name: "journey",
    component: () =>
      import(/* webpackChunkName: "journey" */ "../views/journey.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior: () => {
    return { x: 0, y: 0 };
  },
});

export default router;
