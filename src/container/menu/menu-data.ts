import { Route } from "vue-router";
import { MenuItem } from "./types";

const menus: MenuItem[] = [
  {
    icon: "map-outline",
    label: "SOÜ 景點",
    routePath: "/scenic-spot",
    route: ($route: Route) => {
      return {
        name: "scenic-spot",
        query: { ...$route.query },
      };
    },
  },
  {
    icon: "restaurant-outline",
    label: "SOÜ 美食",
    routePath: "/restaurant",
    route: ($route: Route) => {
      return {
        name: "restaurant",
        query: { ...$route.query },
      };
    },
  },
  {
    icon: "bed-outline",
    label: "SOÜ 旅宿",
    routePath: "/hotel",
    route: ($route: Route) => {
      return {
        name: "hotel",
        query: { ...$route.query },
      };
    },
  },
  {
    icon: "bulb-outline",
    label: "SOÜ 活動",
    routePath: "/activity",
    route: ($route: Route) => {
      return {
        name: "activity",
        query: { ...$route.query },
      };
    },
  },
  {
    icon: "footsteps-outline",
    label: "行程規劃",
    routePath: "/journey",
    route: () => {
      return {
        name: "journey",
      };
    },
  },
];

export default menus;
