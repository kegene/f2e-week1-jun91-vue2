import { Route, Location } from "vue-router";

export interface MenuItem {
  icon: string;
  label: string;
  route: ($route: Route) => Location;
  routePath: string;
  hoverAction?: {
    icon: string;
    label: string;
  };
}
