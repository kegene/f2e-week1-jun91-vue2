import type Vue from "vue";
import type { formatPicture } from "@/api/utils/query";

export declare class DrawerCard extends Vue {
  infoData: Data;
}

export interface Data {
  title: string;
  description: string;
  images?: ReturnType<typeof formatPicture>;
  items: Item[];
}

export interface Item {
  icon: string;
  value: string;
  href?: string;
}
