import { City as CityT, Town } from "@/api/types/basic";

export interface City extends CityT {
  CityID: string;
  CityName: string;
  CityCode: string;
  City: string;
  CountyID: string;
  Version: string;
  Region: RegionType;
}

export interface TownData {
  [key: string]: Town[];
}

export enum RegionType {
  /** 北部 */
  North,
  /** 東部 */
  East,
  /** 中部 */
  Central,
  /** 南部 */
  South,
  /** 離島 */
  OutlyingIslands,
}

export interface CityBlock {
  label: string;
  id: number;
  cityCode: City[];
}

export interface ModelValue {
  country: City;
  town: Town;
}
