export interface Icons {
  name: string;
  classNames?: string[];
  visibility?: boolean | (() => boolean);
  bindAttr?: any;
  on?: Events;
}

export type Events = {
  [key in keyof HTMLElementEventMap]?: (event: Event) => void;
};
