export function formatTime(date: string) {
  if (!date) {
    return "";
  }
  return new Date(date).toLocaleString(undefined, { hour12: false });
}
