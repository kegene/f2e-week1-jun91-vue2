import axios, {
  AxiosRequestConfig,
  AxiosInstance,
  AxiosPromise,
  AxiosResponse,
} from "axios";

const useAxios = (options: AxiosRequestConfig): AxiosInstance => {
  const $axios = axios.create({ ...options });

  // 請求資料之前
  $axios.interceptors.request.use(
    function (config) {
      if (!config.params.$filter) {
        config.params.$filter = undefined;
      }
      config.params.$format = "json";
      return config;
    },
    function (error) {
      return Promise.reject(error);
    }
  );

  // // 返回資料之前
  // $axios.interceptors.response.use(
  //   function (response) {
  //     return response;
  //   },
  //   function (error) {
  //     return Promise.reject(error);
  //   }
  // );
  return $axios;
};

export default useAxios;
export { AxiosPromise, AxiosResponse };
