import imageError from "@/assets/error/image-error.png";

export function onImageError(e: Event) {
  const el = e.target as HTMLImageElement;
  el.src = imageError;
  el.alt = "未提供圖片或是無法讀取圖片";
}
