const env = process.env;
const defaultConfig = {};
const dev = { ...defaultConfig, publicPath: "/" };
const test = { ...defaultConfig, publicPath: "/dist" };
const prod = { ...defaultConfig, publicPath: "/f2e-week1-jun91-vue2" };

const config =
  env.NODE_ENV === "production" ? prod : env.NODE_ENV === "test" ? test : dev;

module.exports = config;
