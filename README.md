# f2e 第一週 觀光導覽（TDX 觀光、旅宿、餐飲 api）
`vue 2` `vuex` `typescript` `axios`

![](img-meadme-iindex.png)

## 簡介
本次網站主題是 關於台灣的觀光旅遊資訊，分為景點、旅宿、餐飲、活動四大類型。

### DEMO

https://kegene.gitlab.io/f2e-week1-jun91-vue2/

## 項目資訊

- Node 12.16
- Vue 2
- Vuex 3.4
- Axios
- Eslint
- Stylus
- Typescript
- jsSHA

**項目運行**

```
$ yarn
$ yarn serve
```

## 功能開發

- [x] 臺灣地區下拉功能
- [x] 側滑彈窗功能
- [x] RWD 版面樣式
- [ ] 手勢輪播功能
- [ ] 周遭景點、餐廳、旅宿資訊顯示功能
- [ ] 行程規劃
  - [x] 加入收藏清單功能
  - [ ] 依據收藏項目，開啟谷歌導航 多個停留點地圖功能
## 感謝名單

-  設計稿 來自 Web UI 設計師 jun91 ([設計稿link | The F2E 精神時光屋](https://2021.thef2e.com/users/6296427084285739695?week=1&type=1))
- 資料來源 來自 TDX運輸資料流通服務平臺 提供之api

